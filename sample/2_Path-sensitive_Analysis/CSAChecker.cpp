
#include <iostream>
#include <utility>

#include <clang/StaticAnalyzer/Core/BugReporter/BugType.h>
#include <clang/StaticAnalyzer/Core/Checker.h>
#include <clang/StaticAnalyzer/Frontend/CheckerRegistry.h>

#include <clang/StaticAnalyzer/Core/PathSensitive/CallEvent.h>
#include <clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h>

using namespace std;
using namespace llvm;
using namespace clang;
using namespace ento;


struct FileState {
private:
    enum State {Open, Closed} state;
    explicit FileState (State s): state{s} {}
public:
    static FileState getOpen() {return FileState(Open);}
    static FileState getClosed() {return FileState(Closed);}

    bool isOpen() const {return state == Open;}
    bool isClosed() const {return state == Closed;}

    bool operator==(const FileState &rhs) const {
        return state == rhs.state;
    }

    void Profile(FoldingSetNodeID &id) const {
        id.AddInteger(state);
    }
};


class FileStateChecker: public Checker<check::PostCall, check::PreCall, check::DeadSymbols, check::PointerEscape> {
public:
    FileStateChecker();
    void checkPostCall(const CallEvent &, CheckerContext &) const;
    void checkPreCall(const CallEvent &, CheckerContext &) const;
    void checkDeadSymbols(SymbolReaper &, CheckerContext &) const;
    ProgramStateRef checkPointerEscape(
            ProgramStateRef, const InvalidatedSymbols &, const CallEvent *, PointerEscapeKind) const;

private:
    unique_ptr<BugType> leakedBugType;

    const CallDescription OpenFn = CallDescription("fopen");
    const CallDescription CloseFn = CallDescription("fclose", 1);

};


REGISTER_MAP_WITH_PROGRAMSTATE(FileStateMap, SymbolRef, FileState)


FileStateChecker::FileStateChecker() {
    leakedBugType = make_unique<BugType>(this, "Potential Leak", "Unix Stream API Bug", true);
}


void FileStateChecker::checkPostCall(const CallEvent &cevt, CheckerContext &cctxt) const {
    if (!cevt.isGlobalCFunction())
        return;
    if (!cevt.isCalled(OpenFn))
        return;

    auto fileHandler = cevt.getReturnValue().getAsSymbol();
    if (!fileHandler)
        return;

    ProgramStateRef progState = cctxt.getState();
    progState = progState->set<FileStateMap>(fileHandler, FileState::getOpen());
    cctxt.addTransition(progState);
}


void FileStateChecker::checkPreCall(const CallEvent &cevt, CheckerContext &cctxt) const {
    if (!cevt.isGlobalCFunction())
        return;
    if (!cevt.isCalled(CloseFn))
        return;

    auto fileHandler = cevt.getArgSVal(0).getAsSymbol();
    if (!fileHandler)
        return;

    progState = progState->set<FileStateMap>(fileHandler, FileState::getClosed());
    cctxt.addTransition(progState);
}


void FileStateChecker::checkDeadSymbols(SymbolReaper &sr, CheckerContext &cctxt) const {
    ProgramStateRef progState = cctxt.getState();
    SmallVector<SymbolRef, 2> leakedStreams;
    FileStateMapTy stateMap = progState->get<FileStateMap>();
    for (auto iter = stateMap.begin(); iter != stateMap.end(); iter++) {

        if (sr.isDead(iter->first) && iter->second.isOpen()) {
            ConstraintManager &cmgr = cctxt.getConstraintManager();
            ConditionTruthVal condTVal = cmgr.isNull(progState, iter->first);
            if (!condTVal.isConstrainedTrue()) {
                leakedStreams.push_back(iter->first);
            }
        }

        if (sr.isDead(iter->first))
            progState = progState->remove<FileStateMap>(iter->first);
    }

    auto *nd = cctxt.generateNonFatalErrorNode(progState);
    if (!nd)
        return;
    for (auto leakedStream : leakedStreams) {
        auto report = make_unique<PathSensitiveBugReport>(*leakedBugType, "Potential Leak", nd);
        report->markInteresting(leakedStream);
        cctxt.emitReport(move(report));
    }
}


static bool guaranteedNotToCloseFile(const CallEvent &cevt) {
    if (!cevt.isInSystemHeader())
        return false;
    if (cevt.argumentsMayEscape())
        return false;
    return true;
}

ProgramStateRef FileStateChecker::checkPointerEscape(
        ProgramStateRef progState, const InvalidatedSymbols &escape, const CallEvent *cevt, PointerEscapeKind kind) const {
    if (kind == PSK_DirectEscapeOnCall && guaranteedNotToCloseFile(*cevt))
        return progState;

    for (auto sym : escape)
        progState = progState->remove<FileStateMap>(sym);

    return progState;
}

extern "C" void clang_registerCheckers(CheckerRegistry &registry) {
    registry.addChecker<FileStateChecker>("file-stream-check","check leak", "");
}

extern "C" const char clang_analyzerAPIVersionString[] = CLANG_ANALYZER_API_VERSION_STRING;
