#include <iostream>

#include <clang/AST/AST.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/ASTConsumer.h>
#include <clang/ASTMatchers/ASTMatchers.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>

#include <clang/Basic/SourceManager.h>

#include <clang/Rewrite/Core/Rewriter.h>

#include <clang/Frontend/FrontendPluginRegistry.h>
#include <clang/Frontend/CompilerInstance.h>

#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>

#include <llvm/Support/CommandLine.h>

using namespace std;
using namespace clang;
using namespace tooling;
using namespace ast_matchers;
using namespace llvm;


class ObfuscatorCallBack: public MatchFinder::MatchCallback {
public:
    explicit ObfuscatorCallBack(shared_ptr<Rewriter> r): rewriter{move(r)} {}

    void run(const MatchFinder::MatchResult &res) override {
        auto *ctx = res.Context;
        auto nde = res.Nodes;

        auto *binOperator = nde.getNodeAs<BinaryOperator>("bop");
        auto *binLHS = nde.getNodeAs<Expr>("lhs");
        auto *binRHS = nde.getNodeAs<Expr>("rhs");

        string lhsStr = getOperandString(binLHS);
        string rhsStr = getOperandString(binRHS);

        auto lhsRange = binLHS->getSourceRange();
        auto rhsRange = binRHS->getSourceRange();

        string lhsRewriteStr, rhsRewriteStr;

        if (binOperator->getOpcodeStr().str() == "+") {
            lhsRewriteStr = "(" + lhsStr + " ^ " + rhsStr + ")";
            rhsRewriteStr = "2 * (" + lhsStr + " & " + rhsStr + ")";
        }
        else if (binOperator->getOpcodeStr().str() == "-") {
            lhsRewriteStr = "(" + lhsStr + " + ~" + rhsStr + ")";
            rhsRewriteStr = "1";
            rewriter->ReplaceText(binOperator->getOperatorLoc(), "+");
        }

        rewriter->ReplaceText(lhsRange, lhsRewriteStr);
        rewriter->ReplaceText(rhsRange, rhsRewriteStr);

    }

private:
    shared_ptr<Rewriter> rewriter;

    static string getOperandString(const Expr *exp) {
        if (auto *impCastExp = dyn_cast<ImplicitCastExpr>(exp)) {
            if (auto *declRefExp = dyn_cast<DeclRefExpr>(impCastExp->getSubExpr()))
                return declRefExp->getDecl()->getNameAsString();
        }
        else if (auto *integerExp = dyn_cast<IntegerLiteral>(exp))
            return to_string(integerExp->getValue().getZExtValue());

        return "";
    }
};


class ObfuscatorASTConsumer: public ASTConsumer {
public:
    explicit ObfuscatorASTConsumer(shared_ptr<Rewriter> r): obfHandler{ObfuscatorCallBack(r)} {
        auto binMatcher = binaryOperator(
                hasAnyOperatorName(StringRef("+"), StringRef("-")),
                hasLHS(
                        anyOf(implicitCastExpr(hasImplicitDestinationType(isSignedInteger())).bind("lhs"),
                                integerLiteral().bind("lhs"))),
                hasRHS(
                        anyOf(implicitCastExpr(hasImplicitDestinationType(isSignedInteger())).bind("rhs"),
                                integerLiteral().bind("rhs")))
                ).bind("bop");

        matcher.addMatcher(binMatcher, &obfHandler);
    }

    void HandleTranslationUnit(ASTContext &ctx) override {
        matcher.matchAST(ctx);
    }

private:
    ObfuscatorCallBack obfHandler;
    MatchFinder matcher;
};


class ObfuscatorAction: public PluginASTAction {
public:
    explicit ObfuscatorAction(): rewriter{make_shared<Rewriter>()} {}

    unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override {
        rewriter->setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return make_unique<ObfuscatorASTConsumer>(rewriter);
    }

    bool ParseArgs(const CompilerInstance &CI, const vector<string> &argv) override {
        return true;
    }

    void EndSourceFileAction() override {
        rewriter->getEditBuffer(rewriter->getSourceMgr().getMainFileID()).write(outs());
    }

private:
    shared_ptr<Rewriter> rewriter;
};


//int main(int argc, char **argv) {
//    if (argc > 1) {
//        //tooling::runToolOnCode(make_unique<ObfuscatorAction>(), argv[1]);
//
//        static cl::OptionCategory obfOptCat("Obfuscator");
//        CommonOptionsParser optParser(argc, (const char**)argv, obfOptCat);
//        ClangTool obfscator(optParser.getCompilations(), optParser.getSourcePathList());
//        auto obfFactory = newFrontendActionFactory<ObfuscatorAction>();
//        obfscator.run(obfFactory.get());
//    }
//    return 0;
//}

static FrontendPluginRegistry::Add<ObfuscatorAction> X("-obf-subs", "obfuscate substraction");
