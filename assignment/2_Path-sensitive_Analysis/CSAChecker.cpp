
#include <iostream>
#include <utility>

#include <clang/StaticAnalyzer/Core/BugReporter/BugType.h>
#include <clang/StaticAnalyzer/Core/Checker.h>
#include <clang/StaticAnalyzer/Frontend/CheckerRegistry.h>

#include <clang/StaticAnalyzer/Core/PathSensitive/CallEvent.h>
#include <clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h>

using namespace std;
using namespace llvm;
using namespace clang;
using namespace ento;


struct FileState {
private:
    enum State {Open, Closed} state;
    explicit FileState (State s): state{s} {}
public:
    static FileState getOpen() {return FileState(Open);}
    static FileState getClosed() {return FileState(Closed);}

    bool isOpen() const {return state == Open;}
    bool isClosed() const {return state == Closed;}

    bool operator==(const FileState &rhs) const {
        return state == rhs.state;
    }

    void Profile(FoldingSetNodeID &id) const {
        id.AddInteger(state);
    }
};


class FileStateChecker: public Checker<check::PostCall, check::PreCall> {
public:
    FileStateChecker();
    void checkPostCall(const CallEvent &, CheckerContext &) const;
    void checkPreCall(const CallEvent &, CheckerContext &) const;

private:
    unique_ptr<BugType> doubleCloseBugType;

    const CallDescription OpenFn = CallDescription("fopen");
    const CallDescription CloseFn = CallDescription("fclose", 1);

};


REGISTER_MAP_WITH_PROGRAMSTATE(FileStateMap, SymbolRef, FileState)


FileStateChecker::FileStateChecker() {
    doubleCloseBugType = make_unique<BugType>(this, "Double Close", "Unix Stream API Bug");
}


void FileStateChecker::checkPostCall(const CallEvent &cevt, CheckerContext &cctxt) const {
    if (!cevt.isGlobalCFunction())
        return;
    if (!cevt.isCalled(OpenFn))
        return;

    auto fileHandler = cevt.getReturnValue().getAsSymbol();
    if (!fileHandler)
        return;

    ProgramStateRef progState = cctxt.getState();
    progState = progState->set<FileStateMap>(fileHandler, FileState::getOpen());
    cctxt.addTransition(progState);
}


void FileStateChecker::checkPreCall(const CallEvent &cevt, CheckerContext &cctxt) const {
    if (!cevt.isGlobalCFunction())
        return;
    if (!cevt.isCalled(CloseFn))
        return;

    auto fileHandler = cevt.getArgSVal(0).getAsSymbol();
    if (!fileHandler)
        return;

    ProgramStateRef progState = cctxt.getState();
    const FileState *fs = progState->get<FileStateMap>(fileHandler);
    if (fs && fs->isClosed()) {
        cout << cevt.getSourceRange().getBegin().printToString(cctxt.getSourceManager()) << endl;
        return;
    }

    progState = progState->set<FileStateMap>(fileHandler, FileState::getClosed());
    cctxt.addTransition(progState);
}


extern "C" void clang_registerCheckers(CheckerRegistry &registry) {
    registry.addChecker<FileStateChecker>("double-close-check","check double close", "");
}

extern "C" const char clang_analyzerAPIVersionString[] = CLANG_ANALYZER_API_VERSION_STRING;
