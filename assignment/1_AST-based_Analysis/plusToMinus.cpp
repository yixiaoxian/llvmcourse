//
// Created by REI on 2021/2/17.
//

#include <iostream>
#include <tuple>
#include <string>

#include <clang/AST/AST.h>
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/ASTContext.h>
#include <clang/ASTMatchers/ASTMatchers.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>

#include <clang/Rewrite/Core/Rewriter.h>

#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendPluginRegistry.h>

#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>

#include <llvm/Support/CommandLine.h>

using namespace std;
using namespace clang;
using namespace ast_matchers;
using namespace llvm;

class PlusToMinus: public MatchFinder::MatchCallback {
public:

    explicit PlusToMinus(shared_ptr<Rewriter> r): rewriter{move(r)} {}

    void run(const MatchFinder::MatchResult &res) override {
        auto *ctx = res.Context;
        auto nd = res.Nodes;

        auto *addOperator = nd.getNodeAs<BinaryOperator>("add");
        auto *lhs = addOperator->getLHS();
        auto *rhs = addOperator->getRHS();

        ParenExpr *parenExp;
        UnaryOperator *unaryExp;

        auto rhsTuple = isAddMinusOpr(rhs);
        parenExp = get<0>(rhsTuple);
        unaryExp = get<1>(rhsTuple);
        if (parenExp && unaryExp) {
            rewriter->ReplaceText(addOperator->getOperatorLoc(), "-");
            if (auto *impCastExp = dyn_cast<ImplicitCastExpr>(unaryExp->getSubExpr())) {
                string oprStr = dyn_cast<DeclRefExpr>(impCastExp->IgnoreImplicit())->getDecl()->getNameAsString();
                rewriter->ReplaceText(parenExp->getSourceRange(), oprStr);
            }
            else if (auto *integerExp = dyn_cast<IntegerLiteral>(unaryExp->getSubExpr())) {
                string oprStr = to_string(integerExp->getValue().getZExtValue());
                rewriter->ReplaceText(parenExp->getSourceRange(), oprStr);
            } else
                llvm_unreachable("Unsupported case");
        }
    }

private:
    shared_ptr<Rewriter> rewriter;
    static tuple<ParenExpr*, UnaryOperator*> isAddMinusOpr(Expr *exp) {
        if (auto *parenExp = dyn_cast<ParenExpr>(exp)) {
            if (auto *unaryExp = dyn_cast<UnaryOperator>(parenExp->getSubExpr())) {
                if (UnaryOperator::getOpcodeStr(unaryExp->getOpcode()) == "-") {
                    return make_tuple(parenExp, unaryExp);
                }
            }
        }
        return make_tuple(nullptr, nullptr);
    }

};


class PlusToMinusConsumer: public ASTConsumer {
public:
    explicit PlusToMinusConsumer(shared_ptr<Rewriter> r): plusToMinusHandler(r) {
        auto plusToMinusMatcher = binaryOperator(hasOperatorName("+")).bind("add");
        matcher.addMatcher(plusToMinusMatcher, &plusToMinusHandler);
    }

    void HandleTranslationUnit(ASTContext &ctx) override {
        matcher.matchAST(ctx);
    }

private:
    MatchFinder matcher;
    PlusToMinus plusToMinusHandler;
};


class PlusToMinusAction: public PluginASTAction {
public:

    PlusToMinusAction(): rewriter{make_shared<Rewriter>()} {}

    unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override {
        rewriter->setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return make_unique<PlusToMinusConsumer>(rewriter);
    }

    void EndSourceFileAction() override {
        rewriter->getEditBuffer(rewriter->getSourceMgr().getMainFileID()).write(llvm::outs());
    }

    bool ParseArgs(const CompilerInstance &CI, const vector<string> &args) override {
        return true;
    }

private:
    shared_ptr<Rewriter> rewriter;
};


static FrontendPluginRegistry::Add<PlusToMinusAction> X("rewrite-plus-minus", "rewrite plus minus");

